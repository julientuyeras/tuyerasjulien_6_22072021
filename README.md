
![Logo](https://i.ibb.co/ctQ2RKd/Capture-d-cran-2021-08-16-11-45-02.png)

    
# Projet 6 - formation Dév Web - Openclassrooms - So Pekocko


Construisez une API sécurisée pour une application d'avis gastronomiques


* **Brief :**

La marque So Pekocko, qui crée des sauces piquantes, connaît un franc succès, en partie grâce à sa chaîne de vidéos YouTube “La piquante”.
L’entreprise souhaite désormais développer une application d’évaluation de ses sauces piquantes, appelée “Piquante”.
Même si l’application deviendra peut-être un magasin en ligne dans un futur proche, Sophie, la product owner de So Pekocko, a décidé que le MVP du projet sera une application web permettant aux utilisateurs d’ajouter leurs sauces préférées et de liker ou disliker les sauces ajoutées par les autres utilisateurs.


## Technos utilisés

Framework : Express

Serveur : NodeJS

Base de données : MongoDB

JS, CSS, HTML

utilisation du pack Mongoose avec des shemas de données
## Pour faire fonctionner le projet :

**Dans un premier temps :**
- FrontEnd :

Pour faire fonctionner le projet :

Vous devez installer depuis le front:

NodeJS en version 12.14 ou 14.0
Angular CLI en version 7.0.2.
node-sass : attention à prendre la version conode-sass -vrrespondante à NodeJS. Pour Node 14.0 par exemple, installer node-sass en version 4.14+.
Sur Windows, ces installations nécessitent d'utiliser PowerShell en tant qu'administrateur.

Démarrer ng serve (ou npm start) pour avoir accès au serveur de développement. Rendez-vous sur http://localhost:4200/. L'application va se recharger automatiquement si vous modifiez un fichier source.

depuis le dossier "front-end"
```bash
  npm start
```

  
**Dans un deuxieme temps :**
- BackEnd :

Modifiez le fichier .env.template en .env
*renseignez vos champs :
- User
- Password
- Url Host
(présent dans votre base de données MongoDB)

Installez nodemon depuis le back-end avec 

```bash
  npm install -g nodemon.
```

puis lancer nodemon
```bash
  nodemon
```

**Et voilà 😊**



  
## Fait par

- [@julientuyeras](https://www.gitlab.com/julientuyeras)

